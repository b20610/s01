import java.util.Scanner;

public class Main {
    /*
        Main Class

        The Main class is the entry point for our Java program.
        It is responsible for executing our code.
        The main class usually has 1 method inside it, the main() method.
            The main method is the method to run our code.

        ctrl + / - single comments
        ctrl + shift + / - multi-line comments
    */
    public static void main(String[] args){
        /*
            Main Method is where most executable code is applied to.

            "public" is an access modifier which simply tells the application which classes have access to our method/attributes

            "static" means that the method/property belongs to the class. This means that it is accessible without having to create an instance of a class.

            "void" means that this method will not return data. Because in Java we have to declare data type of the method's return. And since, main does not return data, we add "void".
        */
        //System.out.println() is a statement which will allow us to print the  value of the argument passed into it in our terminal.
        System.out.println("Josef Ortega");
        // sout + tab - shortcut to type System.out.println instantly
        System.out.println("I am learning Java!");

        //In Java, to be able to declare a variable, we have to identify or declare its data type. Which means, that the variable will expect and only accept data with the tyoe declared:

        int myNum;
        myNum = 3;
        System.out.println(myNum);

        myNum = 2500;
        System.out.println(myNum);

        int nationalPopulation = 2147483647;
        System.out.println(nationalPopulation);

        // nationalPopulation = 2147483647;

        // L is added at the end of a long number to be recognized as long. Otherwise, it will be wrongfully recognized as int.
        long worldPopulation = 7862881145L;

        // We have to add f at the end of a float to be recognized as float.
        float piFloat = 3.141592f;
        System.out.println(piFloat);

        double piDouble = 3.1415926;
        System.out.println(piDouble);

        //char - can only hold 1 character
        // ' ' is a character literal. only 1 letter
        char letter = 'a';
        System.out.println(letter);

        // "" is a string literal.
        // letter = "b";

        // boolean
        boolean isMVP = true;
        boolean isChampion = false;
        System.out.println(isMVP);
        System.out.println(isChampion);

        // contants in Java is declared with final keyword and the data type
        // final dataType VARIABLENAME
        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);

        //String - In Java, is a non-primitive data type. This is because strings are actually objects that can use methods.
        //Non-primitive data types have access to methods:
        //Array,Class,Interface

        String username = "yusukeUrameshi91";
        System.out.println(username);

        //String username2 = 'a'; error because single quote is for char

        //Strings being non-primitive have access to methods.
        //.isEmpty() is a string method which returns a boolean.
        //It will check the length of the string, returns true if the length is 0, returns false if otherwise:
        System.out.println(username.isEmpty());

        String username2 = "";
        System.out.println(username2.isEmpty());

        //Scanner - is a class in Java which allows us to create an instance that accepts input from the terminal. It's like prompt() in JS.
        //However, Scanner being a class pre-defined by Java, has to be imported to be used.

        Scanner scannerName = new Scanner(System.in);

        //.nextLine() receives user input and returns a string.
        System.out.println("What is your name?");
        String myName = scannerName.nextLine();

        System.out.println("Your name is " + myName + "!");

        //.nextInt() receives user input and returns an integer type
        System.out.println("What is your favorite number?");
        int myFaveNum = scannerName.nextInt();

        System.out.println(myFaveNum);

        //.nextDouble() receives user input and returns a double type:
        System.out.println("What was your average grade in HS?");
        double aveGrade = scannerName.nextDouble();

        System.out.println(aveGrade);

        //Much like in JS, Java also has similar mathematical operators
        System.out.println("Enter the first number:");
        int number1 = scannerName.nextInt();

        System.out.println("Enter the second number:");
        int number2 = scannerName.nextInt();

        int sum = number1 + number2;
        System.out.println("The sum of both numbers are: " + sum);

        /*
            Mini-Activity

            1. Add a system out print line to ask the user for a number.
                - create a new integer variable and store the value returned by using the scanner and its nextInt() method.
            2. Add a system out print line to ask the suer for another number.
                - create a new integer variable and store the value returned by using the scanner and its nextInt() method.
            3. Create a new integer variable to store the difference of the numbers that was input.
            4. Print the value of the difference between the input numbers.
        */

        System.out.println("Enter first number: ");
        int number3 = scannerName.nextInt();

        System.out.println("Enter second number: ");
        int number4 = scannerName.nextInt();

        int diff = number3 - number4;
        System.out.println("The difference of both numbers are: " + diff);
    }

}