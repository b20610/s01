import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner userInput = new Scanner(System.in);

        System.out.println("First Name:");
        String firstName = userInput.nextLine();

        System.out.println("Last Name:");
        String lastName = userInput.nextLine();

        System.out.println("First Subject Grade:");
        Double firstSubject = userInput.nextDouble();

        System.out.println("Second Subject Grade:");
        Double secondSubject = userInput.nextDouble();

        System.out.println("Third Subject Grade:");
        Double thirdSubject = userInput.nextDouble();

        System.out.println("Good day, " + firstName + " " + lastName);

        Double average = (firstSubject + secondSubject + thirdSubject)/3;

        System.out.println("Your grade average is: " + average);


    }
}